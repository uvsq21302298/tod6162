package tod6162;

public class Fraction implements Comparable<Fraction>
{
	public static void main(String[] args){}

	private final int num;
    private final int denum;
    
    public static Fraction ZERO = new Fraction();
    public static Fraction UN = new Fraction(1, 1);
    
    public Fraction(int num, int denum)
    {
		this.num = num;
        if(denum == 0) {
            throw new ArithmeticException();
        }
        this.denum = denum;
    }
    public Fraction(int num)
    {
		this(num, 1);
    }
	public Fraction()
    {
		this(0, 1);
    }
    
    public int getNum(){ return num; }
    public int getDenum(){ return denum; }
    

    public double doubleValue(){
		return ((double) num / (double) denum);
	}
	
    @Override
    public String toString()
    {
        if(denum != 1) {
            return Integer.valueOf(num).toString() + " / " + Integer.valueOf(denum).toString();
        }
        else {
            return Integer.valueOf(num).toString();
        }
    }
    public static int pgcd(int a,int b) {
        int r = a;
        //dernier reste non nul
        while (r != 0) {
            r = a%b;
            a=b;
            b=r;
        }
        return a;
    }
    
    public static Fraction getReducedFraction(int num, int denum){
		int pgcd = pgcd(num,denum);
		if(pgcd == 0)
			return Fraction.ZERO;
		else
			return new Fraction(num/pgcd, denum/pgcd);
	}
	
	 public Fraction add(Fraction f)
    {
        return getReducedFraction(num * f.denum + denum * f.num, denum * f.getDenum());
    }
    
    @Override
    public boolean equals(Object o)
    {
        if(o instanceof Fraction) {
            Fraction f1 = getReducedFraction(num, denum);
            Fraction f2 = getReducedFraction(((Fraction) o).getNum(), ((Fraction) o).getDenum());
            
            return f1.getNum() == f2.getNum() && f1.getDenum() == f2.getDenum();
        }
        else {
            return false;
        }
    }
    
    public int compareTo(Fraction o)
    {
        //a - b = a + (-b)
        Fraction f1 = getReducedFraction(- o.getNum(), o.getDenum()); // b = - b
        Fraction f2 = this.add(f1); // a - b
        
        if(f2.getNum() == 0) return 0;
        return Math.abs(f2.getNum() * f2.getDenum()) / (f2.getNum() * f2.getDenum());
          
    }
    
}
